# test

serverless invoke local --function create --path mocks/create-event.json --profile default

serverless invoke local --function get --path mocks/get-event.json --profile default

serverless invoke local --function list --path mocks/list-event.json --profile default

serverless invoke local --function update --path mocks/update-event.json --profile default

serverless invoke local --function delete --path mocks/delete-event.json --profile default

# deploy

serverless deploy --aws-profile myProfile

serverless deploy function -f list

# cognito identity pool

`Note: make sure config this policy to Cognito_slsstackpoolAuth_Role`

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      "Resource": ["*"]
    },
    {
      "Effect": "Allow",
      "Action": ["s3:*"],
      "Resource": [
        "arn:aws:s3:::sls-stack-bucket/private/${cognito-identity.amazonaws.com:sub}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": ["execute-api:Invoke"],
      "Resource": ["arn:aws:execute-api:ap-southeast-2:*:u1vozp93gk/*/*/*"]
    }
  ]
}
```

`YOUR_S3_UPLOADS_BUCKET_NAME:`
sls-stack-bucket
`YOUR_COGNITO_USER_POOL_ID:`
ap-southeast-2_V6Rhjccx4
`YOUR_COGNITO_APP_CLIENT_ID:`
1tgqap9rsdme7g7i5hmqj3s0ls
`YOUR_COGNITO_REGION:`
ap-southeast-2
`YOUR_IDENTITY_POOL_ID:`
ap-southeast-2:ae07812e-4240-4c98-aef2-e4f052740a8c
`YOUR_API_GATEWAY_URL:`
https://u1vozp93gk.execute-api.ap-southeast-2.amazonaws.com/prod

# test api

```bash
npx aws-api-gateway-cli-test \
--username='danielhymn@gmail.com' \
--password='123123' \
--user-pool-id='ap-southeast-2_V6Rhjccx4' \
--app-client-id='1tgqap9rsdme7g7i5hmqj3s0ls' \
--cognito-region='ap-southeast-2' \
--identity-pool-id='ap-southeast-2:ae07812e-4240-4c98-aef2-e4f052740a8c' \
--invoke-url='https://u1vozp93gk.execute-api.ap-southeast-2.amazonaws.com/prod/' \
--api-gateway-region='ap-southeast-2' \
--path-template='/notes' \
--method='POST' \
--body='{"content":"hello world","attachment":"hello.jpg"}'
```

# role policy

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "mobileanalytics:PutEvents",
        "cognito-sync:*",
        "cognito-identity:*"
      ],
      "Resource": ["*"]
    },
    {
      "Effect": "Allow",
      "Action": ["s3:*"],
      "Resource": [
        "arn:aws:s3:::sls-stack-bucket/private/${cognito-identity.amazonaws.com:sub}/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": ["execute-api:Invoke"],
      "Resource": ["arn:aws:execute-api:ap-southeast-2:*:u1vozp93gk/*/*/*"]
    }
  ]
}
```
